<?php

namespace SymfonyBro\SecurityExtensionBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use SymfonyBro\SecurityExtensionBundle\DependencyInjection\Compiler\ExtendedSecurityPass;

class SymfonyBroSecurityExtensionBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ExtendedSecurityPass());
    }
}
