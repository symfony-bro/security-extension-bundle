<?php
namespace SymfonyBro\SecurityExtensionBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class ExtendedSecurityPass
 *
 * @package SymfonyBro\SecurityExtensionBundle\DependencyInjection\Compiler
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ExtendedSecurityPass implements CompilerPassInterface
{
    const SECURITY_EXPRESSION_LANGUAGE = 'security.expression_language';
    const SENSIO_FRAMEWORK_EXTRA_SECURITY_EXPRESSION_LANGUAGE_DEFAULT = 'sensio_framework_extra.security.expression_language.default';

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $providerDefinition = $container->getDefinition('symfony_bro_security_extension.security.extended_security_functions_provider');

        if($container->hasDefinition(self::SENSIO_FRAMEWORK_EXTRA_SECURITY_EXPRESSION_LANGUAGE_DEFAULT)) {
            $definition = $container->getDefinition(self::SENSIO_FRAMEWORK_EXTRA_SECURITY_EXPRESSION_LANGUAGE_DEFAULT);
            $definition->addMethodCall('registerProvider', [$providerDefinition]);
        }

        if($container->hasDefinition(self::SECURITY_EXPRESSION_LANGUAGE)) {
            $definition = $container->getDefinition(self::SECURITY_EXPRESSION_LANGUAGE);
            $definition->addMethodCall('registerProvider', [$providerDefinition]);
        }
    }
}
