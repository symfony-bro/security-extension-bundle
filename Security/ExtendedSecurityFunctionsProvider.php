<?php

namespace SymfonyBro\SecurityExtensionBundle\Security;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;

/**
 * Class ExtendedSecurityFunctionsProvider
 *
 * @package SymfonyBro\SecurityExtensionBundle\Security
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class ExtendedSecurityFunctionsProvider implements ExpressionFunctionProviderInterface
{

    /**
     * @var ExtendedAuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(ExtendedAuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @return ExpressionFunction[] An array of Function instances
     */
    public function getFunctions()
    {
        return array(
            new ExpressionFunction('is_granted', function ($attributes, $object = 'null') {
                return sprintf('$auth_checker->isGranted(%s, %s)', $attributes, $object);
            }, function (array $variables, $attributes, $object = null) {
                return $this->authorizationChecker->isGranted($attributes, $object);
            }),
            new ExpressionFunction('is_granted_all', function ($attributes, $object = 'null') {
                return sprintf('$auth_checker->isGrantedAll(%s, %s)', $attributes, $object);
            }, function (array $variables, $attributes, $object = null) {
                return $this->authorizationChecker->isGrantedAll($attributes, $object);
            }),
            new ExpressionFunction('is_granted_all_except', function ($attributes, $except, $object = 'null') {
                return sprintf('$auth_checker->isGrantedAllExcept(%s, %s, %s)', $attributes, $except, $object);
            }, function (array $variables, $attributes, $except, $object = null) {
                return $this->authorizationChecker->isGrantedAllExcept($attributes, $except, $object);
            }),
            new ExpressionFunction('is_granted_except', function ($attributes, $except, $object = 'null') {
                return sprintf('$auth_checker->isGrantedExcept(%s, %s, %s)', $attributes, $except, $object);
            }, function (array $variables, $attributes, $except, $object = null) {
                return $this->authorizationChecker->isGrantedExcept($attributes, $except, $object);
            }),
        );
    }
}
