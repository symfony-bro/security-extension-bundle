<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\SecurityExtensionBundle\Security;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

interface ExtendedAuthorizationCheckerInterface extends AuthorizationCheckerInterface
{
    public function isGrantedAll($attributes, $object = null);

    public function isGrantedAllExcept($attributes, $exceptAttributes, $object = null);

    public function isGrantedExcept($attributes, $exceptAttributes, $object = null);
}
