<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\SecurityExtensionBundle\Security;


use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AuthorizationCheckerDecorator implements ExtendedAuthorizationCheckerInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Checks if the attributes are granted against the current authentication token and optionally supplied object.
     *
     * @param mixed $attributes
     * @param mixed $object
     *
     * @return bool
     */
    public function isGranted($attributes, $object = null)
    {
        return $this->authorizationChecker->isGranted($attributes, $object);
    }

    public function isGrantedAll($attributes, $object = null)
    {
        if (!is_array($attributes)) {
            $attributes = array($attributes);
        }
        $f = true;
        $attribute = reset($attributes);
        while ($attribute && ($f = $this->authorizationChecker->isGranted($attribute, $object))) {
            $attribute = next($attributes);
        }

        return $f;
    }

    public function isGrantedAllExcept($attributes, $exceptAttributes, $object = null)
    {
        $f = $this->isGrantedAll($attributes, $object);

        if($f) {
            $f = !$this->authorizationChecker->isGranted($exceptAttributes, $object);
        }

        return $f;
    }

    public function isGrantedExcept($attributes, $exceptAttributes, $object = null)
    {
        $f = $this->authorizationChecker->isGranted($attributes, $object);

        if($f) {
            $f = !$this->authorizationChecker->isGranted($exceptAttributes, $object);
        }

        return $f;
    }
}
