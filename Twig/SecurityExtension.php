<?php

namespace SymfonyBro\SecurityExtensionBundle\Twig;

use SymfonyBro\SecurityExtensionBundle\Security\ExtendedAuthorizationCheckerInterface;

/**
 * Class IsGrantedAllExtension
 *
 * @package SymfonyBro\SecurityExtensionBundle\Twig
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */
class SecurityExtension extends \Twig_Extension
{
    /**
     * @var ExtendedAuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(ExtendedAuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('is_granted_all', function ($attributes, $object = null) {
                return $this->authorizationChecker->isGrantedAll($attributes, $object);
            }),
            new \Twig_SimpleFunction('is_granted_all_except', function ($attributes, $except, $object = null) {
                return $this->authorizationChecker->isGrantedAllExcept($attributes, $except, $object);
            }),
            new \Twig_SimpleFunction('is_granted_except', function ($attributes, $except, $object = null) {
                return $this->authorizationChecker->isGrantedExcept($attributes, $except, $object);
            }),
        ];
    }
}
