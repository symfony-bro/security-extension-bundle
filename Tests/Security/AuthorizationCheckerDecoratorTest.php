<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\SecurityExtensionBundle\Tests\Security;


use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use SymfonyBro\SecurityExtensionBundle\Security\AuthorizationCheckerDecorator;

class AuthorizationCheckerDecoratorTest extends TestCase
{
    public function testIsGranted()
    {
        $decoratedAuthChecker = $this->getAuthCheckerMock();

        $decoratedAuthChecker->method("isGranted")
            ->withConsecutive(['ROLE_FOO', null])
            ->willReturnOnConsecutiveCalls(true);
        $authChecker = new AuthorizationCheckerDecorator($decoratedAuthChecker);
        $this->assertTrue($authChecker->isGranted('ROLE_FOO'));
    }

    public function testIsGrantedAll()
    {
        $decoratedAuthChecker = $this->getAuthCheckerMock();

        $decoratedAuthChecker->method("isGranted")
            ->withConsecutive(['ROLE_FOO', null], ['ROLE_BAR', null], ['ROLE_FOO', null], ['ROLE_BAR', null])
            ->willReturnOnConsecutiveCalls(true, true, true, false);
        $authChecker = new AuthorizationCheckerDecorator($decoratedAuthChecker);
        $this->assertTrue($authChecker->isGrantedAll(['ROLE_FOO', 'ROLE_BAR']));
        $this->assertFalse($authChecker->isGrantedAll(['ROLE_FOO', 'ROLE_BAR']));
    }

    public function testIsGrantedAllWithEmptyAttributes()
    {
        $decoratedAuthChecker = $this->getAuthCheckerMock();

        $decoratedAuthChecker->expects($this->never())->method("isGranted");

        $authChecker = new AuthorizationCheckerDecorator($decoratedAuthChecker);
        $this->assertTrue($authChecker->isGrantedAll([]));
    }

    public function testIsGrantedAllExcept()
    {
        $decoratedAuthChecker = $this->getAuthCheckerMock();
        $decoratedAuthChecker->method("isGranted")
            ->withConsecutive(['ROLE_FOO', null], ['ROLE_BAR', null], ['ROLE_FOO', null], ['ROLE_BAR', null], ['ROLE_FOO', null], ['ROLE_BUZ', null], ['ROLE_BAR', null])
            ->willReturnOnConsecutiveCalls(true, true, true, false, true, true, false);
        $authChecker = new AuthorizationCheckerDecorator($decoratedAuthChecker);

        $this->assertFalse($authChecker->isGrantedAllExcept('ROLE_FOO', 'ROLE_BAR'));
        $this->assertTrue($authChecker->isGrantedAllExcept('ROLE_FOO', 'ROLE_BAR'));
        $this->assertTrue($authChecker->isGrantedAllExcept(['ROLE_FOO', 'ROLE_BUZ'], 'ROLE_BAR'));
    }


    public function testIsGrantedAllExceptEmptyAttributes()
    {
        $decoratedAuthChecker = $this->getAuthCheckerMock();
        $decoratedAuthChecker->method("isGranted")
            ->withConsecutive(['ROLE_BAR', null])
            ->willReturnOnConsecutiveCalls(false);
        $authChecker = new AuthorizationCheckerDecorator($decoratedAuthChecker);

        $this->assertTrue($authChecker->isGrantedAllExcept([], 'ROLE_BAR'));
    }

    public function testIsGrantedExcept()
    {
        $decoratedAuthChecker = $this->getAuthCheckerMock();
        $decoratedAuthChecker->method("isGranted")
            ->withConsecutive(
                [['ROLE_FOO', 'ROLE_BAR'], null], ['ROLE_BAZ', null],
                [['ROLE_FOO', 'ROLE_BAR'], null],
                [['ROLE_FOO', 'ROLE_BAR'], null], ['ROLE_BAZ', null]
            )
            ->willReturnOnConsecutiveCalls(
                true, false,
                false,
                true, true
            );

        $authChecker = new AuthorizationCheckerDecorator($decoratedAuthChecker);
        $this->assertTrue($authChecker->isGrantedExcept(['ROLE_FOO', 'ROLE_BAR'], 'ROLE_BAZ'));
        $this->assertFalse($authChecker->isGrantedExcept(['ROLE_FOO', 'ROLE_BAR'], 'ROLE_BAZ'));
        $this->assertFalse($authChecker->isGrantedExcept(['ROLE_FOO', 'ROLE_BAR'], 'ROLE_BAZ'));
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|AuthorizationCheckerInterface
     */
    private function getAuthCheckerMock()
    {
        return $this->getMockForAbstractClass(AuthorizationCheckerInterface::class);
    }


}
