<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\SecurityExtensionBundle\Tests\Security;


use PHPUnit\Framework\TestCase;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use SymfonyBro\SecurityExtensionBundle\Security\AuthorizationCheckerDecorator;
use SymfonyBro\SecurityExtensionBundle\Security\ExtendedSecurityFunctionsProvider;

class ExtendedSecurityFunctionProviderTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function testProvider($result, $expression, $isGrantedArguments, $isGrantedResult)
    {
        $decoratedAuthChecker = $this->getAuthCheckerMock();
        $decoratedAuthChecker->method("isGranted")
            ->withConsecutive(...$isGrantedArguments)
            ->willReturnOnConsecutiveCalls(...$isGrantedResult);

        $authChecker = new AuthorizationCheckerDecorator($decoratedAuthChecker);
        $language = new ExpressionLanguage();
        $language->registerProvider(new ExtendedSecurityFunctionsProvider($authChecker));

        $context = array();
        $context['auth_checker'] = $authChecker;

        $this->assertEquals($result, $language->evaluate($expression, $context));
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|AuthorizationCheckerInterface
     */
    private function getAuthCheckerMock()
    {
        return $this->getMockForAbstractClass(AuthorizationCheckerInterface::class);
    }

    public function provider()
    {
        return [
            [true, 'is_granted_all("ROLE_FOO")', [['ROLE_FOO', null]], [true]],
            [false, 'is_granted_all(["ROLE_FOO", "ROLE_BAR"])', [['ROLE_FOO', null], ['ROLE_BAR', null]], [true, false]],
//            [true, 'is_granted_all_except(["ROLE_FOO", "ROLE_BAR"], ["ROLE_BUZ"])', [['ROLE_FOO', null], ['ROLE_BAR', null], ['ROLE_BUZ', null]], [true, true, false]]
        ];
    }
}
